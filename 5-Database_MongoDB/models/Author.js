const mongoose = require("mongoose");

const AuthorSchema = new mongoose.Schema({
    firstname: String,
    name: String,
});

module.exports = mongoose.model("Author", AuthorSchema);