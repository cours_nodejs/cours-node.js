const mongoose = require("mongoose");
const Author = require("./models/Author");

async function getRows() {
    await mongoose.connect(`mongodb://localhost:27017/cours`);
    const rows = await Author.find();
    return rows;
}

getRows().then((res) => {
    console.log(res);
})
.catch((error) => {
    console.log(error);
})