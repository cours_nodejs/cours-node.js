const { Pool } = require("pg");

const pool = new Pool({
    "user": "pierre",
    "host": "localhost",
    "database": "pierre",
    "password": "MyPassword",
});

async function getRows() {
    const { rows } = await pool.query("SELECT * FROM author");
    return rows;
}

getRows().then((res) => {
    console.log(res);
})
.catch((error) => {
    console.log(error);
})