const express = require("express");

const app = express();
const router = express.Router();

router.get("/", (req, res) => {
    return res.status(200).send({
        'status': 'ok',
    });
});

router.post("/addition", (req, res) => {
    const body = req.body;
    const { var1, var2 } = body;
    return res.status(200).send({
        'result': var1 + var2,
    });
});

app.use(express.json());
app.use(router);

app.listen(3000, () => {
    console.log("SERVER RUNNING ON PORT 3000");
});