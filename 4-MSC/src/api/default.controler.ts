import express, { Request, Response } from "express";
import { Data } from "../interfaces/body.interface";

import DefaultService from "../services/default.service";

const defaultService = new DefaultService();

export const defaultRouter = express.Router();

defaultRouter.get("/", (req: Request, res: Response) => {
    try {
        const data: Data[] = defaultService.getAll();
        return res.status(200).send(data);
    } catch (error: any) {
        return res.status(500).send(error.message);
    }
});