import { Data } from "../interfaces/body.interface";
import DefaultModel from "../models/default.model";

const defaultModel = new DefaultModel();

export default class DefaultService {

    getAll(): Data[] {
        return defaultModel.getAll();
    }
}