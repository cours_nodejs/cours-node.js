"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
const router = express_1.default.Router();
router.get("/", (req, res) => {
    try {
        const value = 3;
        return res.status(200).send(value);
    }
    catch (error) {
        console.error(error);
        return res.status(500).send(error.message);
    }
});
router.post("/body", (req, res) => {
    try {
        const body = req.body;
        return res.status(200).send(body.val1);
    }
    catch (error) {
        console.error(error);
        return res.status(500).send(error.message);
    }
});
app.use(express_1.default.json());
app.use(router);
