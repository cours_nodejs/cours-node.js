function functionWithCallback(var1, var2, callback) {
    setTimeout(() => {
        const var3 = var1 + var2;
        callback(var3);
    }, 1000);
}

console.log("Hello");
functionWithCallback(1, 2, (result) => {
    console.log(result);
})
console.log("World");