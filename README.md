# Cours Node.JS

Par Pierre HUGUES

## Liens Utiles

- [Doc Node.JS](https://nodejs.org/docs/latest/api/)
- [API REST Node.JS](https://practicalprogramming.fr/node-js-api)
- [Architecture Node.JS](https://scoutapm.com/blog/nodejs-architecture-and-12-best-practices-for-nodejs-development)
- [Node.JS + PostgreSQL](https://node-postgres.com/)
- [Node.JS + MongoDB](https://mongoosejs.com/)
