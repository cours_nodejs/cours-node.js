import express from "express";

import { getHostName } from "./utils.js";

const app = express();
const router = express.Router();

router.get("/hostname", (req, res) => {
    try {
        const hostname = getHostName();
        return res.status(200).send(hostname);
    } catch(error) {
        console.error(error);
        return res.status(500).send(error.message);
    }
});

app.use(express.json());
app.use(router);

app.listen(3000, () => {
    console.log("SERVER RUNNING ON PORT 3000");
});