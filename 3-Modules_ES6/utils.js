import { hostname } from "os";

export function getHostName() {
    return hostname();
}